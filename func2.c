// Command Shell FrameWork for GBC
// Function 2


#include "func2.h"

// Initilizer for startup
UBYTE func2_init()
{
	// Button titler is fixed.
	dispStrings(FUNC2_TITLE_X, FUNC2_TITLE_Y, FUNC2_TITLE);

	//Mod
	// Your routine should be put on here
	// ...
	//

	return 0;
}

UBYTE func2(UBYTE keyStroke)
{
	//Mod
	// Function body 
	switch(keyStroke){
		case KEY_MAKE_MAKE:
			// Long Press can not be existed with edge-behavior
			if(!gLongPressCounter[FUNCTION_2]--){
				// Long press action
			}
		break;
		case KEY_MAKE_BREAK:
		break;
		case KEY_BREAK_MAKE:
		break;
	}


	return 0;
}
