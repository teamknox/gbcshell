// Command Shell FrameWork for GBC
// Function 5


#include "func5.h"

// Initilizer for startup
UBYTE func5_init()
{
	// Button titler is fixed.
	dispStrings(FUNC5_TITLE_X, FUNC5_TITLE_Y, FUNC5_TITLE);

	//Mod
	// Your routine should be put on here
	// ...
	//

	return 0;
}

UBYTE func5(UBYTE keyStroke)
{
	//Mod
	// Function body 
	switch(keyStroke){
		case KEY_MAKE_MAKE:
			// Long Press can not be existed with edge-behavior
			if(!gLongPressCounter[FUNCTION_5]--){
				// Long press action
			}
		break;
		case KEY_MAKE_BREAK:
		break;
		case KEY_BREAK_MAKE:
		break;
	}


	return 0;
}
