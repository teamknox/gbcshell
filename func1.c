// Command Shell FrameWork for GBC
// Function 1


#include "func1.h"

// Initilizer for startup
UBYTE func1_init()
{
	// Button titler is fixed.
	dispStrings(FUNC1_TITLE_X, FUNC1_TITLE_Y, FUNC1_TITLE);

	//Mod
	// Your routine should be put on here
	// ...
	//

	return 0;
}

UBYTE func1(UBYTE keyStroke)
{
	//Mod
	// Function body 
	switch(keyStroke){
		case KEY_MAKE_MAKE:
			// Long Press can not be existed with edge-behavior
			if(!gLongPressCounter[FUNCTION_1]--){
				// Long press action
			}
		break;
		case KEY_MAKE_BREAK:
		break;
		case KEY_BREAK_MAKE:
		break;
	}


	return 0;
}
