# Launcher-shell for Gameboy color
You have to spend a lot of time for software development on GBC, especially you are novice. We developed framework which is focused of machine controlling. You write only diffrential part. Your code will be assinged to each button. 
<table>
<tr>
<td><img src="./pics/gb-shell.jpg"></td>
</tr>
</table>

## How it works
Your target action will be described to func0.c - func5.c funcX_init() should contain initialize, funcX() has body of each function. 

## How to develop
Download and unzip to your target folder, you have to modify files as below,
* title.h
* target.h
* target.c
* funcX.h
* funcX.c 

# License
Copyright (c) Osamu OHASHI  
Distributed under the MIT License either version 1.0 or any later version. 

