// Command Shell FrameWork for GBC
// Function 4


#include "func4.h"

// Initilizer for startup
UBYTE func4_init()
{
	// Button titler is fixed.
	dispStrings(FUNC4_TITLE_X, FUNC4_TITLE_Y, FUNC4_TITLE);

	//Mod
	// Your routine should be put on here
	// ...
	//

	return 0;
}

UBYTE func4(UBYTE keyStroke)
{
	//Mod
	// Function body 
	switch(keyStroke){
		case KEY_MAKE_MAKE:
			// Long Press can not be existed with edge-behavior
			if(!gLongPressCounter[FUNCTION_4]--){
				// Long press action
			}
		break;
		case KEY_MAKE_BREAK:
		break;
		case KEY_BREAK_MAKE:
		break;
	}


	return 0;
}
