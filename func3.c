// Command Shell FrameWork for GBC
// Function 3


#include "func3.h"

// Initilizer for startup
UBYTE func3_init()
{
	// Button titler is fixed.
	dispStrings(FUNC3_TITLE_X, FUNC3_TITLE_Y, FUNC3_TITLE);

	//Mod
	// Your routine should be put on here
	// ...
	//

	return 0;
}

UBYTE func3(UBYTE keyStroke)
{
	//Mod
	// Function body 
	switch(keyStroke){
		case KEY_MAKE_MAKE:
			// Long Press can not be existed with edge-behavior
			if(!gLongPressCounter[FUNCTION_3]--){
				// Long press action
			}
		break;
		case KEY_MAKE_BREAK:
		break;
		case KEY_BREAK_MAKE:
		break;
	}


	return 0;
}
