// Command Shell FrameWork for GBC
// 2001 TeamKNOx

// Palette
#define CGBPal0c0 32767
#define CGBPal0c1 24311
#define CGBPal0c2 15855
#define CGBPal0c3 0

// Key configuration
#define KEY_STEP_X 80
#define KEY_STEP_Y 24
#define START_CURSOR_X 40
#define START_CURSOR_Y 80

// Function Key configuration
#define UNIT_SIZE_X 1
#define UNIT_SIZE_Y 3

// Cursor configuration
// Type of Cursors
#define CURSOR_RELEASE	0
#define CURSOR_PUSH		4
// Cusor
#define CURSOR_NUMBER	2
#define CURSOR_SIZE		4

#define CURSOR_MOVE_OFFSET	8

//Function behavior
#define KEY_MAKE_MAKE	1
#define KEY_MAKE_BREAK	2
#define KEY_BREAK_MAKE	3


// Function Table
#define FUNCTION_0 0
#define FUNCTION_1 1
#define FUNCTION_2 2
#define FUNCTION_3 3
#define FUNCTION_4 4
#define FUNCTION_5 5

#define FUNCTION_TABLE_X 2
#define FUNCTION_TABLE_Y 3

#define FUNCTION_NUMBERS FUNCTION_TABLE_X * FUNCTION_TABLE_Y

// Display area
#define DISP_UPPER_SIDE	1,3
#define DISP_LOWER_SIDE	1,4
#define DISP_CLEAR_STR "                  "


// Long Press Value
#define LONG_PRESS_VALUE 100UL

// GLOBALS
UWORD pBkgPalette[] = {
	CGBPal0c0, CGBPal0c1, CGBPal0c2, CGBPal0c3
};

// Table definition
UBYTE gFunctionTable[FUNCTION_TABLE_Y][FUNCTION_TABLE_X] = {
	{'0', '3'},
	{'1', '4'},
	{'2', '5'}
};

// for Long key press counting
UBYTE gLongPressCounter[FUNCTION_NUMBERS];

// for general use
char  gWorkStr[20];
UWORD gWorkUW;
UBYTE gWorkUB;
