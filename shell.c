// Command Shell FrameWork for GBC
// 2001 TeamKNOx

#include <gb.h>
#include <stdlib.h>
#include <string.h>

#include "shell.h"

#include "title.h"

// BackGround
#include "bkg.c"

// object
#include "cursor.c"

// Tile coordinatetion
#include "shell_bkg.c"


void init_disp()
{
  disable_interrupts();
  set_bkg_data( 0, 128, bkg );
  SHOW_BKG;
  DISPLAY_ON;
  enable_interrupts();
  set_bkg_tiles( 0, 0, 20, 18, shell_bkg );
  set_bkg_palette( 0, 1, pBkgPalette );
}

void set_cursor(UBYTE cursorType)
{
	UBYTE i;

	set_sprite_data(CURSOR_RELEASE, CURSOR_SIZE * CURSOR_NUMBER, cursor_data);
	for(i = 0;i <= CURSOR_SIZE;i++){
		set_sprite_tile(i, i + cursorType);
	}
	SHOW_SPRITES;
}

void move_cursor(UBYTE x, UBYTE y)
{
	move_sprite(0, x, y);
	move_sprite(1, x, y + CURSOR_MOVE_OFFSET);
	move_sprite(2, x + CURSOR_MOVE_OFFSET, y);
	move_sprite(3, x + CURSOR_MOVE_OFFSET, y + CURSOR_MOVE_OFFSET);
}


UBYTE dispStrings(UBYTE posX, UBYTE posY, char *strings)
{
	UBYTE strLength;
	unsigned char workStr[20];
	
	strLength = 0;
	while(strings[strLength]){
		workStr[strLength] = strings[strLength];
		strLength++;
	}
	set_bkg_tiles(posX, posY, strLength, 1, workStr);
	
	return strLength;
}

void clrDisplay()
{
	dispStrings(DISP_UPPER_SIDE, DISP_CLEAR_STR);
	dispStrings(DISP_LOWER_SIDE, DISP_CLEAR_STR);
}

// "99" -> 99
UWORD str02num(char *stringNumber)
{
	return ((stringNumber[1] - '0') * 10UL) + (stringNumber[0] - '0');
}

// "999" -> 999
UWORD str03num(char *stringNumber)
{
	return (((stringNumber[0] - '0') * 100UL) + ((stringNumber[1] - '0') * 10UL) + (stringNumber[2] - '0'));
}

// "9999" -> 9999
UWORD str04num(char *stringNumber)
{
	return (((stringNumber[0] - '0') * 1000UL) + ((stringNumber[1] - '0') * 100UL) + ((stringNumber[2] - '0') * 10UL) + (stringNumber[3] - '0'));
}


// Number to Strings
void num04str(UWORD number)

{
  UWORD m;
  UBYTE i, n, f;
  char work[6];

  f = 0; m = 1000;
  for( i=0; i<4; i++ ) {
    n = number / m; number = number % m; m = m / 10;
    if( (n==0)&&(f==0) ) {
      work[i] = ' ';		// ' '
//      work[i] = '0';		// '0'
    } else {
      f = 1;
      work[i] = n + '0';	//'0' - '9'
    }
  }
  work[i] = 0x00;

  i = n = 0;
  while(work[n]){
  	while(work[n] == ' '){
  		n++;
  	}
  	gWorkStr[i] = work[n];
  	i++;
  	n++;
  }
  gWorkStr[i] = 0x00;
}



// FrameWork
#include "comm.c"
#include "target.c"

// Plug-Ins


// Your Functtions
#include "func0.c"
#include "func1.c"
#include "func2.c"
#include "func3.c"
#include "func4.c"
#include "func5.c"


int main()
{
	UBYTE key1, key2, i, j, k, pos_x, pos_y, func_num;

	SPRITES_8x8;

	init_disp();
	
	set_cursor(CURSOR_RELEASE);

	dispStrings(0, 1, TITLE_MSG);
	
	i = j = 0;

	//communication driver initialize
	comm_init();
	
	// global initialization
	target_init();

	// function initializer
	func0_init();
	func1_init();
	func2_init();
	func3_init();
	func4_init();
	func5_init();

	move_cursor(i * KEY_STEP_X + START_CURSOR_X, j * KEY_STEP_Y + START_CURSOR_Y);		// cursor appear

	while(1){
		delay(10);
		
		// Frequently execute
		target();
		
		func_num = gFunctionTable[j][i];
		key1 = joypad();
		
		if(key1 != key2){
			pos_x = i * KEY_STEP_X + START_CURSOR_X;
			pos_y = j * KEY_STEP_Y + START_CURSOR_Y;
			move_cursor(pos_x, pos_y);
		}
		
		if(key2 & J_A){
			if(key1 & J_A){
				// Make continuslly
				switch(func_num){
					case '0':
						func0(KEY_MAKE_MAKE);
					break;
					case '1':
						func1(KEY_MAKE_MAKE);
					break;
					case '2':
						func2(KEY_MAKE_MAKE);
					break;
					case '3':
						func3(KEY_MAKE_MAKE);
					break;
					case '4':
						func4(KEY_MAKE_MAKE);
					break;
					case '5':
						func5(KEY_MAKE_MAKE);
					break;
				}
			}
			else{
				// Just released !!
				set_cursor(CURSOR_RELEASE);
				switch(func_num){
					case '0':
						func0(KEY_MAKE_BREAK);
					break;
					case '1':
						func1(KEY_MAKE_BREAK);
					break;
					case '2':
						func2(KEY_MAKE_BREAK);
					break;
					case '3':
						func3(KEY_MAKE_BREAK);
					break;
					case '4':
						func4(KEY_MAKE_BREAK);
					break;
					case '5':
						func5(KEY_MAKE_BREAK);
					break;
				}
			}
		}
		else{
			if(key1 & J_A){
				// Just Make !!
				set_cursor(CURSOR_PUSH);
				switch(func_num){
					case '0':
						func0(KEY_BREAK_MAKE);
					break;
					case '1':
						func1(KEY_BREAK_MAKE);
					break;
					case '2':
						func2(KEY_BREAK_MAKE);
					break;
					case '3':
						func3(KEY_BREAK_MAKE);
					break;
					case '4':
						func4(KEY_BREAK_MAKE);
					break;
					case '5':
						func5(KEY_BREAK_MAKE);
					break;
				}
			}
			else{
				// Still break - action definetion is no meaning.
				// LongPressCounter reset
				for(k = 0;k <= FUNCTION_NUMBERS;k++){
					gLongPressCounter[k] = 0;
				}
			}
		}
		
		if(!(key1 & J_A)){
			if((key1 & J_UP) && !(key2 & J_UP) && j > 0)
				j--;
			else if((key1 & J_DOWN) && !(key2 & J_DOWN) && j < UNIT_SIZE_Y)
				j++;

			if((key1 & J_LEFT) && !(key2 & J_LEFT) && i > 0)
				i--;
			else if((key1 & J_RIGHT) && !(key2 & J_RIGHT) && i < UNIT_SIZE_X)
				i++;
		}
		key2 = key1;
	}

	return 0;

}
