// Command Shell FrameWork for GBC
// Function 0


#include "func0.h"

// Initilizer for startup
UBYTE func0_init()
{
	// Button titler is fixed.
	dispStrings(FUNC0_TITLE_X, FUNC0_TITLE_Y, FUNC0_TITLE);

	//Mod
	// Your routine should be put on here
	// ...
	//

	return 0;
}

UBYTE func0(UBYTE keyStroke)
{
	//Mod
	// Function body 
	switch(keyStroke){
		case KEY_MAKE_MAKE:
			// Long Press can not be existed with edge-behavior
			if(!gLongPressCounter[FUNCTION_0]--){
				// Long press action
			}
		break;
		case KEY_MAKE_BREAK:
		break;
		case KEY_BREAK_MAKE:
		break;
	}


	return 0;
}
